/* eslint-disable prefer-destructuring */
export const API_BASE_PATH = process.env.API_BASE_PATH;
export const TOKEN_KEY = 'craglog_token';
export const CACHE_LIMIT = 30000;
export const DATE_FORMAT = 'MM/dd HH:mm.SS';
/* eslint-enable prefer-destructuring */
