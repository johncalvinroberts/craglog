'use strict';

const path = require('path');
const AutoLoad = require('fastify-autoload');

module.exports = function(fastify, opts, next) {
  // Do not touch the following lines
  // This loads all plugins defined in plugins
  // those should be support plugins that are reused
  // through your application
  fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'plugins'),
    options: { ...opts }
  });

  // This loads all plugins defined in services
  // define your routes in one of these
  fastify.register(AutoLoad, {
    dir: path.join(__dirname, 'services'),
    options: { ...opts, prefix: '/api/v1' }
  });
  // Make sure to call next when done
  next();
};
