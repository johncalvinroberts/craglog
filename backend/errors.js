'use strict';

module.exports = {
  USERNAME_IS_NOT_AVAILABLE: { key: 'USERNAME_IS_NOT_AVAILABLE', code: 412 },
  WRONG_CREDENTIAL: { key: 'WRONG_CREDENTIAL', code: 401 },
  ROUTE_EXISTS: { key: 'ROUTE_EXISTS', code: 412 },
  FORBIDDEN: { key: 'FORBIDDEN', code: 403 }
};
